package com.sapient.forecast.service;

import com.sapient.forecast.model.Response;

public interface WeatherService {

	
	public Response getWeatherForecast( String city);
}
