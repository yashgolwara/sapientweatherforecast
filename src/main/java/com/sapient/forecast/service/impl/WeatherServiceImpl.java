package com.sapient.forecast.service.impl;

import java.net.URI;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import com.sapient.forecast.model.Response;
import com.sapient.forecast.model.WeatherEntry;
import com.sapient.forecast.model.WeatherForecast;
import com.sapient.forecast.service.WeatherService;

@Service
public class WeatherServiceImpl implements WeatherService{ 

	@Value("${app.weather.url}")
	private String WEATHER_URL;
	@Value("${app.forecast.url}")
	private String FORECAST_URL;
	@Value("${app.weather.api.key}")
	private String apiKey;

	private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

	@Autowired
	private RestTemplate restTemplate ;
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	   // Do any additional configuration here
	   return builder.build();
	}
	

	@Override
	public Response getWeatherForecast(String city) {
		logger.info("weather forecast for ",  city);
		URI url = new UriTemplate(FORECAST_URL).expand(city, this.apiKey);
		WeatherForecast forecast= invoke(url, WeatherForecast.class);
		Response response = new Response();
		response.setName(forecast.getName());
		List<HashMap<String , String>> allDetails = new ArrayList<HashMap<String,String>>();
		List<WeatherEntry> entries = forecast.getEntries();
		DateFormat df = DateFormat.getDateTimeInstance();
		df = new SimpleDateFormat("dd");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String currentDate = df.format(new Date());
		int date = Integer.parseInt(currentDate);
		for (int i =0; i<entries.size() ; i++) {
			Timestamp timestamp = Timestamp.from(entries.get(i).getTimestamp());
			if(date == Integer.parseInt(df.format(timestamp))) {
				HashMap<String , String> dayDetails = new HashMap<String, String>();
				dayDetails.put("maxTemp", entries.get(i).getTemp_max());
				dayDetails.put("minTemp", entries.get(i).getTemp_min());
				dayDetails.put("date",entries.get(i).getTimestamp().toString());
				dayDetails.put("rain",entries.get(i).getRain());
				
				if(Double.parseDouble(entries.get(i).getTemp_max()) > 40.00) {
					dayDetails.put("message","Use sunscreen lotion");
				}else if(entries.get(i).getRain().toLowerCase().contains("clouds")) {
					dayDetails.put("message","Carry Umbrella");
				}else {
					dayDetails.put("message","Have a good day");
				}
				if(allDetails.size()<3) 
			allDetails.add(dayDetails);
				else 
					break;
			date ++;}
		response.setWeatherDetails(allDetails);	
		response.setSuccess(true);
		logger.debug("Response of weather forecast for ",  response.toString());
		}
		
		return response;
	}

	private <T> T invoke(URI url, Class<T> responseType) throws HttpClientErrorException {
		
		RequestEntity<?> request = RequestEntity.get(url).accept(MediaType.APPLICATION_JSON).build();
		ResponseEntity<T> exchange = restTemplate.exchange(request, responseType);
		return exchange.getBody();
	}

}
