package com.sapient.forecast.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.forecast.model.Response;
import com.sapient.forecast.service.WeatherService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Sapient Weather Forecast")
@RestController
@RequestMapping("/api/weather")
public class WeatherApiController {
	
	@Autowired
	WeatherService weatherService;
	

	@ApiOperation(value = "Public method to get weather forecast", response = Response.class)
	@ApiResponses(value = {
            @ApiResponse(code = 200, message = "ok"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found") })
	@RequestMapping("/cities/{city}")
	public Response getWeatherForecast(@PathVariable String city) {
		
		return weatherService.getWeatherForecast(city);
	}

}
