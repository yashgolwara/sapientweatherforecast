package com.sapient.forecast.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class WeatherEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Instant timestamp;

	private String temperature;

	private String temp_min;
	
	private String temp_max;
	
	private String rain;
	
	

	@JsonProperty("timestamp")
	public Instant getTimestamp() {
		return this.timestamp;
	}

	@JsonSetter("dt")
	public void setTimestamp(long unixTime) {
		this.timestamp = Instant.ofEpochMilli(unixTime * 1000);
	}

	
	public String getTemperature() {
		return this.temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	
	

	public String getTemp_min() {
		return temp_min;
	}

	public void setTemp_min(String temp_min) {
		this.temp_min = temp_min;
	}

	public String getTemp_max() {
		return temp_max;
	}

	public void setTemp_max(String temp_max) {
		this.temp_max = temp_max;
	}

	@JsonProperty("main")
	public void setMain(Map<String, Object> main) {
		setTemperature(getCelsiusTemperature(Double.parseDouble(main.get("temp").toString())));
		setTemp_max(getCelsiusTemperature(Double.parseDouble(main.get("temp_max").toString())));
		setTemp_min(getCelsiusTemperature(Double.parseDouble(main.get("temp_min").toString())));
	}

	
	public String getRain() {
		return rain;
	}

	public void setRain(String rain) {
		this.rain = rain;
	}

	@JsonProperty("weather")
	public void setWeather(ArrayList<Map<String, Object>> weatherEntries) {
		Map<String, Object> weather = weatherEntries.get(0);
		setRain(weather.get("main").toString());
	}



	
	
	public String getCelsiusTemperature(Double temperature) {
		double celsiusTemp = temperature - 273.15;
		return String.format("%4.2f", celsiusTemp);
	}


}
