package com.sapient.forecast.model;

import java.util.HashMap;
import java.util.List;

public class Response {
	
	
	private List<HashMap<String, String>> weatherDetails;
	
	private String name;
	
	private boolean success;

	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<HashMap<String, String>> getWeatherDetails() {
		return weatherDetails;
	}

	public void setWeatherDetails(List<HashMap<String, String>> allDetails) {
		this.weatherDetails = allDetails;
	}
	
	
	
	
	
	
}
