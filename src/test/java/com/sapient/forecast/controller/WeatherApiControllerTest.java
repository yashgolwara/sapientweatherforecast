package com.sapient.forecast.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import com.sapient.forecast.model.Response;
import com.sapient.forecast.service.WeatherService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = WeatherApiController.class)
//@OverrideAutoConfiguration(enabled = true)
class WeatherApiControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private WeatherService weatherService;

	private Response response;
	
	@MockBean
	private RestTemplate restTemplate;

	@Test
	void testGetWeatherForecast() throws Exception {
		response = new Response();
		response.setName("Delhi");
		List<HashMap<String, String>> allDetails = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> dayDetails = new HashMap<String, String>();
		dayDetails.put("maxTemp", "23.00");
		dayDetails.put("minTemp", "21.00");
		dayDetails.put("rain", "clouds");
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;
		dayDetails.put("date", formatter.format(Instant.now().atZone(ZoneId.systemDefault())));
		dayDetails.put("message", "Use sunscreen lotion");
		allDetails.add(dayDetails);
		response.setWeatherDetails(allDetails);
		response.setSuccess(true);
		when(weatherService.getWeatherForecast("Delhi")).thenReturn(response);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/weather/cities/Delhi");
		mockMvc.perform(requestBuilder).andExpect(status().isOk());

	}
	
	
	
	}


