package com.sapient.forecast.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sapient.forecast.model.Response;


@RunWith(SpringRunner.class)
@SpringBootTest
class WeatherServiceImplTest {
	
	
	@Autowired
	private WeatherService weatherService;
	
	
	
	

	@Test
	void testGetWeatherForecast() {
		Response weatherResponse = weatherService.getWeatherForecast("Delhi");
		assertThat(weatherResponse.getName()).isEqualTo("Delhi");
		assertEquals(true, weatherResponse.isSuccess());

	}

}
